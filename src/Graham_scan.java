import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.*;

class Points {
	int x;
	int y;

	public Points(int a, int b) {
		this.x = a;
		this.y = b;
	}

	public void toPrint() {
		System.out.printf("[%d, %d]\n", x, y);
	}

}


public class Graham_scan {

	static int line_num = 0;
	static int check_num = 0;
	static String line = null;
	static ArrayList<Points> solution = new ArrayList<>();
	static ArrayList<Points> whole = new ArrayList<>();
	static Map<Float, Integer> map = new HashMap<>();

	public static void main(String[] args) throws IOException {

		String filepath = "/Users/samzhang/Desktop/eclipse/CSE355/sample.txt";
		// input your file path in above sentence

		try {

			FileReader fileReader = new FileReader(filepath);

			BufferedReader bufferedReader = new BufferedReader(fileReader);
			line = bufferedReader.readLine();
			line = line.replace("[", "");
			line = line.replace("]", "");
			line = line.replace(" ", "");
			line_num = Integer.parseInt(line);


			while ((line = bufferedReader.readLine()) != null) {

				check_num++;
				line = line.replace("[", "");
				line = line.replace("]", "");
				line = line.replace(" ", "");
				String[] temp = line.split(",");

				int a = Integer.parseInt(temp[0]);
				int b = Integer.parseInt(temp[1]);

				whole.add(new Points(a, b));

			}

			bufferedReader.close(); // stop read file

		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file \"" + filepath + "\"");
		} catch (IOException ex) {
			System.out.println("Error reading file \"" + filepath + "\"");

		}

		if(check_num!=line_num) {
			System.out.printf("You claim there should be %d points, but in your file just have %d points.", line_num,check_num);
			return;
		}
		
		Points p = whole.get(0);
		for (int i = 1; i < line_num; i++) {

			if (p.x > whole.get(i).x) {
				p = whole.get(i);
				continue;
			}
			if (p.x == whole.get(i).x && p.y > whole.get(i).y) {
				p = whole.get(i);
				continue;
			}

		}

		whole.remove(p);
		solution.add(0, p);

		ArrayList<Points> found_same_x = new ArrayList<>();
		ArrayList<Points> high = new ArrayList<>();

		for (int i = 0; i < whole.size(); i++) {

			Points temp = whole.get(i);
			if (temp.x == p.x) {
				found_same_x.add(temp);
				if (high.size() == 0)
					high.add(temp);
				if (high.get(0).y < temp.y) {
					high.remove(0);
					high.add(temp);
				}

			}

		}

		int n = found_same_x.size();
		if (n != 0)
			solution.add(high.get(0));
		int m = 0;
		while (n != 0) {
			whole.remove(found_same_x.get(m));
			m++;
			n--;
		}

		for (int i = 0; i < whole.size(); i++) {

			float ss = slope(p.x, p.y, whole.get(i).x, whole.get(i).y);

			if (!map.containsKey(ss))
				map.put(ss, i);
			else {
				Points p1 = whole.get(map.get(ss));
				Points p2 = whole.get(i);
				int a = (p1.x - p.x) * (p1.x - p.x) + (p1.y - p.y) * (p1.y - p.y);
				int b = (p2.x - p.x) * (p2.x - p.x) + (p2.y - p.y) * (p2.y - p.y);
				if (b > a)
					map.put(ss, i);

			}
		}

		TreeMap<Float, Integer> sorted = new TreeMap<>(map);

		boolean mark = false;
		for (Map.Entry<Float, Integer> entry : sorted.entrySet()) {

			if (mark == false) {
				solution.add(0, whole.get(entry.getValue()));
				mark = true;
				continue;
			}

			Points top = solution.get(0);
			Points sec = solution.get(1);
			Points np = whole.get(entry.getValue());
			int r = ori(sec, top, np);

			while (r == 1) {
				solution.remove(0);
				top = solution.get(0);
				sec = solution.get(1);
				r = ori(sec, top, np);

			}
			if (r == 0) {
				solution.remove(0);
				solution.add(0, np);
				continue;
			}
			if (r == 2) {
				solution.add(0, np);
				continue;
			}
		}

		if (high.size() != 0&& (whole.size()!=0)) {
			
			Points h = high.get(0);
			Points top = solution.get(0);
			Points sec = solution.get(1);

			int r = ori(sec, top, h);

			while (r == 1) {
				solution.remove(0);
				top = solution.get(0);
				sec = solution.get(1);
				r = ori(sec, top, h);
			}
			if (r == 0) {
				solution.remove(0);
			}
		}
		
	 
		
			Collections.reverse(solution);
			
			
			FileWriter fw = new FileWriter("out.txt");
			 
			fw.write("The solution has "+ solution.size() +" points.\n");
			fw.write("The points set of convex hull are:\n");
			
			System.out.printf("The solution has %d points.\n",solution.size());
			System.out.println("The points set of convex hull are:");
			for (int i = 0; i < solution.size(); i++) {
				solution.get(i).toPrint();
				fw.write("["+solution.get(i).x+", "+solution.get(i).y+"]\n");
				
			}
			fw.close();

		
		

	}

	public static int ori(Points a, Points b, Points c) {
		int result = (b.y - a.y) * (c.x - b.x) - (b.x - a.x) * (c.y - b.y);
		if (result == 0)
			return 0;
		return (result > 0) ? 1 : 2;
	}

	public static float slope(int a, int b, int c, int d) {
		float result = (float) (d - b) / (c - a);
		return result;
	}

}
