## Graham Scan implementation

Sam Zhang

This is an implementation about **Graham Scan** based on Java 8.

When you input the point set, it will return the **Convex Hull** points in counter-clockwise.


1. How to run this project?

    I will suggest to use the Eclipse to run this project with Java8. Or NetBeans is another choice.<br/>
    In Eclipse, you can click the "File" button then choose "Import Projects from File System or Archive". In the path, you just choose the file folder where you unzipped.<br/>
    In NetBeans, you can click the "File" button, then choose "Import Project"->"Eclipse project", and click the option 2 "Import Project ignoring Project Dependencies". After that, both path choose the file folder where you unzipped.

2. How to test this project?

    This project is designed to read a .txt file. The first line is the total number of points. The each following lines is corresponding to excatly one point. There are some possible input example:

    3
    [1,2]
    [23,5]
    [44,2]

    4
    2,4
    2,5
    0,8
    22,-4

    Be careful. The x and y coordinate need to be integer value. And each .txt file only can be used for one point set testing.

    Then, you can change the 36th line code to get your own .txt file's path in Graham_scan.java.

3. The output

    It will give you two kinds output. One way is that printing in the Console directly. 
    The other way is write an out.txt file in this project work directory.

 
